.. image:: https://img.shields.io/badge/licence-GPL--3-blue.svg
   :target: http://www.gnu.org/licenses/gpl
   :alt: License: AGPL-3


====================
Export Journal POS
====================

Export des journaux pour import dans outil comptable pour POS


Credits
=======

Contributors
------------

* Juliana Poudou <juliana@le-filament.com>

Maintainer
----------

.. image:: https://le-filament.com/img/logo-lefilament.png
   :alt: Le Filament
   :target: https://le-filament.com

This module is maintained by Le Filament
