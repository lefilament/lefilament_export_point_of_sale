# Copyright 2019 Le Filament (<http://www.le-filament.com>)
# License GPL-3 or later (http://www.gnu.org/licenses/gpl.html).

from odoo import models, fields, http
from odoo.addons.web.controllers.main import serialize_exception
from odoo.http import request
from odoo.addons.lefilament_export_journal.wizard.datas_export import AccountDatasExport
from odoo.addons.lefilament_export_journal.wizard.datas_export import HEADER_DEFAULT
from odoo.addons.lefilament_export_journal.wizard.datas_export import HEADER_DEFAULT_CEGID


class POSDatasExportWizard(models.TransientModel):
    _inherit = "invoice.line.export"

    journal = fields.Selection(selection_add=[('caisses', 'Caisses')])


class POSDatasExport(AccountDatasExport):

    def datas_export_caisses(self, format, date_start, date_end,
                             nom_outil_compta):
        if nom_outil_compta == 'ibiza':
            header = HEADER_DEFAULT
        elif nom_outil_compta == 'cegid':
            header = HEADER_DEFAULT_CEGID
        else:
            header = HEADER_DEFAULT
        request.cr.execute("""
            SELECT l.date,
             'PDV' AS journal,
             a.code AS compte,
             '' AS auxiliaire,
             l.debit,
             l.credit,
             (CASE WHEN l.user_type_id = 1
              THEN a.name
              ELSE CONCAT('Vente ',l.ref,' - ',l.name) END) AS libelle,
             m.name AS piece,
             '' AS echeance,
             l.ref AS ref_piece
            FROM account_move_line AS l
            LEFT JOIN account_account AS a ON l.account_id = a.id
            LEFT JOIN account_journal AS j ON l.journal_id = j.id
            LEFT JOIN account_move AS m ON m.id = l.move_id
            WHERE j.code = 'POSS' AND l.date >= %s AND l.date <= %s
            ORDER BY l.date, l.move_id, a.code DESC;
            """, (date_start, date_end))
        lignes_export = request.cr.dictfetchall()
        company_name = request.env['res.company'].search([('id', '=', 1)]).name
        filename_ = (company_name.title().replace(' ', '')
                     + 'Caisses_' + date_start.replace('-', '')
                     + '_' + date_end.replace('-', ''))

        if format == 'csv':
            return self.export_csv(
                lignes_export, header, filename_, nom_outil_compta)

        return self.export_xls(
            lignes_export, header, filename_, nom_outil_compta)

    @http.route('/web/export_journal/', type='http', auth="user")
    @serialize_exception
    def datas_export(self, format, journal, nom_outil_compta,
                     date_start, date_end, **kw):
        if journal == 'caisses':
            return self.datas_export_caisses(format, date_start, date_end,
                                             nom_outil_compta)

        return super(POSDatasExport, self).datas_export(format, journal,
                                                        nom_outil_compta,
                                                        date_start, date_end)
