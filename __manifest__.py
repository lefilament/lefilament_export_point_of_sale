{
    'name': 'Export Journal Point of Sale',
    'summary': 'Export des journaux pour import outil comptable pour POS',
    'description': '''
        Export des journaux pour import dans outils comptable pour POS
        ''',
    'author': 'LE FILAMENT',
    'version': '12.0.1.0.0',
    'license': "AGPL-3",
    'depends': ['lefilament_export_journal', 'point_of_sale'],
    'qweb': [],
    'data': [
    ],
    'auto_install': True,
}
